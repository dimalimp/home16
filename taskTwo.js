// 1-------------------------------
// if

const age = 1900;

if (age > 2021) {
    console.log("вас еще нету");
} if (age > 2010 && age < 2021) {
    console.log("вы зумер");
} else {
    console.log("вы бумер");
}

// 2------------------------------
// свитч

switch (age) {
    case 2021:
        console.log("день добрый");
        break;

    case 2020:
        console.log("день добрый из прошлого");
        break;

    case 2022:
        console.log("день добрый из будущего");
        break;
    default:
        console.log("привет");
}

// 3---------------------------------
// все числа стали равны значению плюс индекс

let str = "du 5 nj 12";
str = str.toUpperCase();
let newStr = "";

for (let i = 0; i < str.length; i++) {
    if (!str[i].includes(' ')) {
        !isNaN(Number(str[i])) ? newStr = newStr + (Number(str[i]) + i) : newStr = newStr + str[i];
    }
    else {
        newStr = newStr + str[i];
    }
}
str = newStr;

// 4-----------------------------------
// если случайное число два раза подряд, то генератор чисел останавливается

let counter = 0;

while (counter !== 2) {
    let num = Math.floor(Math.random() * (4));

    num === 3 ? counter = counter + 1 : counter = 0;

    console.log(num);
}