// 1----------------------------
// удаляет поля со строками

const obj = {
    name: "Dima",
    age: 30,
    lastName: "Ivanov",
    tel: 37529,
}

let sum = "";
let del = "";
for (key in obj) {
    if (typeof obj[key] === "string") {
        if (!sum) {
            sum = obj[key];
        }

        del = del + obj[key];
        delete obj[key];
    }
}

// 2-------------------------------
// добавляет новое поле

obj[sum] = del;

// 3-------------------------------
// копия объекта, проверка есть ли поле "aav"

const cop = { ...obj };

"aav" === cop ? console.log(true) : console.log(false);

// 4--------------------------------
// склейка массивов и добавление элементов

const arra = [21, 15, 65];
const arrb = [3, 52];
const arrc = [arra[0] + arrb[0], ...arra, ...arrb, arra[arra.length - 1] + arrb[arrb.length - 1]];

// 5-------------------------------
// замена маленького значения на большое

let max = 0;
let min = 0;

for (key in arrc) {
    if (arrc[min] > arrc[key]) {
        min = key;
    }
    if (arrc[max] < arrc[key]) {
        max = key;
    }
}

[arrc[max], arrc[min]] = [arrc[min], arrc[max]];