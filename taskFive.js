// 1-----------------------
// рекурсия, возвращает общий возраст

const rec = [{ name: "авпвап", age: 15 }, { name: "ва", age: 30 }];

let arrAge = rec.map((item) => item.age);

const getSum = (arr) => {
    let sum = arr.shift();

    if (arr.length !== 0) {
        sum = sum + getSum(arr);
    }

    return sum;
}

getSum(arrAge);

// 2-------------------------
// замыкание, счетчик

const getCounter = (startNumber) => {
    let amount = startNumber;
    const fg = (number) => {
        number === undefined ? console.log(amount) : (amount = amount + number);
        return fg;
    };
    return fg;
};

const a = getCounter(10);
a(1)(4)(2)()(4)(1)()(345);

// ---------------------------

const getCounter = () => {
    let amount = 0;
    let isDoneNumber;
    const fg = (number) => {
        amount = amount + 1;
        if (amount === number && !isDoneNumber) {
            isDoneNumber = amount;
        }
        if (isDoneNumber) {
            console.log(isDoneNumber);
        }
        return fg;
    };
    return fg;
};

const a = getCounter();
a(2)(3)(3)(1)(5);