// 1-----------------------
// функция

const name = "Dima"

const sayHiOrByOne = (isHi) => {
  isHi === true ? console.log(`hi ${name}`) : console.log(`by ${name}`);
}
sayHiOrByOne(true);

// 2-------------------------
// функция с параметром

const sayHiOrByTwo = (isHi, name) => {
  isHi === true ? console.log("hi " + name) : console.log("by " + name);
}
sayHiOrByTwo(false, "Dima");

// 3--------------------------
// находит маленькое число, если в массиве есть большее число чем во втором аргументе

const arrN = [40, 5, 20, 8, 7];
const getMin = (arr, x) => {
  let min = arr[0];
  for (key in arr) {
    if (arr[key] > x) {
      return;
    }
    if (min >= arr[key]) {
      min = arr[key];
    }
  }
  return min;
}
const minValue = getMin(arrN, 102);

// 4-------------------------
//

const arr = [2, 1, 6, 7];
const arr2 = [1, 8, 4, 6, 5, 11];
const arr3 = [2, 5, 6];

const arrMax = (arr, fun) => {
  let max = arr.sort((a, b) => b - a)[0];
  fun(max)
}

const fun = (max) => {
  console.log(max);
}

const compare = (value) => {
  value === 7 ? console.log(true) : console.log(false);
}

arrMax(arr, fun);
arrMax(arr, compare);
arrMax(arr2, fun);
arrMax(arr2, compare);
arrMax(arr3, fun);
arrMax(arr3, compare);

// 5-------------------------

let users = [{ name: "alan", age: 23 }, { name: "dima", age: 33 }, { name: "sasha", age: 16 }, { name: "dasha", age: 18 }];

users.sort((a, b) => a.age - b.age); // 5. 1 сортировка по возрасту

users = users.filter(item => item.age >= 18); // 5. 2 удаление младше 18

// usersSum = users.map(item => item.name);
// console.log(usersSum.join("")); // 5. 3

const usersSum = users.reduce((total, person) => total + person.name, ''); // 5.3 склейка всех имен

users = users.map((item, i) => {
  return { ...item, age: item.age + i } // 5.4 увеличить возраст на значение их текущего индекса
})

users.forEach((item, index) => {
  console.log(`hi ${item.name}`); // 5. 5 поздароваться со всеми именами
})