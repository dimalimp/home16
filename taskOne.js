// 1--------------------------------
// просто пример с округлением до целого числа

const z = 1;
const a = 4;

const ans = Math.round((z / 9 * (15 ** a)) / (9 - (z ** (a + 2))));

// 2---------------------------------
// ложь или правда, если больше 10

const b = (ans > 10 || isNaN(ans));